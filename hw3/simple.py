from faker import Faker

faker = Faker()
sizes = [1000,10000,100000]
for i, size in enumerate(sizes):
    with open('file'+str(i)+'binary.txt','wb') as file:
        for j in size:
            file.write(bytearray(faker.text(),'utf-8'))

for i, size in enumerate(sizes):
    with open('file'+str(i)+'.txt','wb') as file:
        for j in size:
            file.write(faker.text())