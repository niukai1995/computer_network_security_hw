#!/bin/zsh
echo “What is the file name?” 
read FILE 
echo “What is the password?”
read PASSWORD
echo 'aes-256-cbc'
(time (/usr/local/opt/libressl/bin/openssl enc -aes-256-cbc -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.1.enc"))
echo 'aes-256-gcm'
(time (/usr/local/opt/libressl/bin/openssl enc -aes-256-gcm -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.2.enc"))
