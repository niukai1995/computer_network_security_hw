#!/bin/zsh
echo “What is the file name?” 
read FILE 
echo “What is the password?”
read PASSWORD
echo 'aes-256-cbc'
(time (/usr/local/opt/libressl/bin/openssl enc -aes-256-cbc -d -a -k $PASSWORD -in $FILE.1.enc -out "$FILE.1"))
echo 'aes-256-ecb'
(time (/usr/local/opt/libressl/bin/openssl enc -aes-256-gcm -d -a -k $PASSWORD -in $FILE.2.enc -out "$FILE.2"))
