#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 10:11:59 2019

@author: mark
"""

import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
import time
import itertools


plain_text = b"a secret message"
Files_path = []
# AES + CBC
#------------------------------------------------------------------------------

algs = [algorithms.Camellia, algorithms.AES, algorithms.SEED,\
        algorithms.CAST5, algorithms.TripleDES]
modes = [modes.CBC, modes.CTR, modes.OFB, modes.CFB, modes.CFB8,\
         modes.GCM, modes.XTS,modes.ECB]
def test(alg, mode,plain_text, flag=True):
    '''
    alg: specify which encryption algorithm I use
    mode: encryption mode
    plain_text & flag: a file path if flag is False, content if flag if True
    '''
    padder = padding.PKCS7(alg.block_size).padder()
    padded_data = padder.update(plain_text) + padder.finalize()

    start = time.time()
    backend = default_backend()
    key = os.urandom(int(next(iter(alg.key_sizes))/8))
    iv = os.urandom(int(next(iter(alg.key_sizes))/8))
    cipher = Cipher(alg(key), mode(iv), backend=backend)
    encryptor = cipher.encryptor()
    ct = encryptor.update(padded_data) + encryptor.finalize()
#    print(ct)
    decryptor = cipher.decryptor()
    ans = decryptor.update(ct) + decryptor.finalize()
    
    end = time.time()
    information = str(alg) + ' ' + str(mode) + ' ' + str(end - start)
    
    return information,ans

error_num = 0
errors=[]
for alg, mode in itertools.product(algs,modes):
#    print(alg,mode)
    
    try:
        x = test(alg, mode, b"a secret message")
    except:
#        print(alg, mode)
        errors.append((alg, mode))
#        print('------------------')
        error_num +=1
for data in itertools.product(algs,modes):
    if data not in errors:
        print(data)
print(error_num)
#    print(x)
