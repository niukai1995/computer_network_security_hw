#!/bin/zsh
echo “What is the file name?” 
read FILE 
echo “What is the password?”
read PASSWORD
echo 'aes-256-cbc'
(time (openssl enc -aes-256-cbc -d -a -k $PASSWORD -in $FILE.1.enc -out "$FILE.1"))
echo 'aes-256-ecb'
(time (openssl enc -aes-256-ecb -d -a -k $PASSWORD -in $FILE.2.enc -out "$FILE.2"))
echo 'des-ecb'
(time (openssl enc -des-ecb -d -a -k $PASSWORD -in $FILE.3.enc -out "$FILE.3"))
echo 'des-cbc'
(time (openssl enc -des-cbc -d -a -k $PASSWORD -in $FILE.4.enc -out "$FILE.4"))
echo 'bf-ebc'
(time (openssl enc -bf-ecb -d -a -md md5 -k PASSWORD -in $FILE.5.enc -out "$FILE.5"))
echo 'bf-cbc'
(time (openssl enc -bf-cbc -d -a -md md5 -k $PASSWORD -in $FILE.6.enc -out "$FILE.6"))
echo 'camellia-128-cbc'
(time (openssl enc -camellia-128-cbc -salt -d -a -md md5 -k PASSWORD -in $FILE.7.enc -out "$FILE.7"))
echo 'camellia-128-ecb'
(time (openssl enc -camellia-128-ecb -salt -d -a -md md5 -k PASSWORD -in $FILE.8.enc -out "$FILE.8"))
