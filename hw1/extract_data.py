#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 14:32:28 2019

@author: mark
"""
import pandas as pd
'''
output: a tuple, (attribute_name, Series object)
'''
def extract(input_name, attribute_name):
    f = open(input_name,'r')
    data = f.readlines()
    
    # clean data
    data = [i.strip() for i in data]
    result = {}
    for i in range(0,len(data),4):
        t = data[i+1].split()[1][2:-1]
        result[data[i]] = float(t)
    f.close()
    return (attribute_name, pd.Series(result))

a1,b1 = extract('f4_en.txt','en')
a2,b2 = extract('f4_de.txt','de')
a3,b3 = extract('f1_en.txt','en')
a4,b4 = extract('f1_de.txt','de')
a5,b5 = extract('f3_en.txt','en')
a6,b6 = extract('f3_de.txt','de')
s1 = 10
s2 = 46
s3 = 108
data = pd.DataFrame()
data['file1'] = b4/b3
data['file2'] = b6/b5
data['file3'] = b2/b1