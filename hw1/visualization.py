#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 16:34:39 2019

@author: mark
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 14:28:15 2019

@author: mark
"""

import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import random
# Import Data
tmp_data = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/gdppercap.csv")
df = data.sort_values('file1')
data = df
a1 = 'file1'
a2 = 'file2'
a3 = 'file3'


# draw line
# https://stackoverflow.com/questions/36470343/how-to-draw-a-line-with-matplotlib/36479941
def newline(p1, p2, color='black'):
    ax = plt.gca()
    l = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]], color='red' if p1[1]-p2[1] > 0 else 'green', marker='o', markersize=6)
    ax.add_line(l)
    return l

fig, ax = plt.subplots(1,1,figsize=(14,14), dpi= 80)

# Vertical Lines
ax.vlines(x=0, ymin=0, ymax=1.25, color='black', alpha=0.7, linewidth=1, linestyles='dotted')
ax.vlines(x=2, ymin=0, ymax=2, color='black', alpha=0.7, linewidth=1, linestyles='dotted')
ax.vlines(x=4, ymin=0, ymax=2, color='black', alpha=0.7, linewidth=1, linestyles='dotted')

# Points
ax.scatter(y=df[a1], x=np.repeat(0, df.shape[0]), s=10, color='black', alpha=0.7)
ax.scatter(y=df[a2], x=np.repeat(2, df.shape[0]), s=10, color='black', alpha=0.7)
ax.scatter(y=df[a3], x=np.repeat(4, df.shape[0]), s=10, color='black', alpha=0.7)

old_x = old_y = 1e9 # make an impossibly large initial offset
thresh = 2 #make a distance threshold
flip=1
for label, x in zip(data.index, data[a1]):
    #calculate distance
    d = ((x-old_x)**2+(1-1)**2)**(.5)
    print(d,label,x)
    #if distance less than thresh then flip the arrow
    if d < thresh: flip*=-1
    print('flit',flip)
    random_x = random.random()
    random_y = random.random()
    plt.annotate(
        label,
        xy = (0, x), xytext = (-150*flip*random_x, 150*flip*random_y),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    old_x = x
flip=1
for label, x in zip(data.index, data[a2]):
    #calculate distance
    d = ((x-old_x)**2+(1-1)**2)**(.5)
    print(d,label,x)
    #if distance less than thresh then flip the arrow
    if d < thresh: flip*=-1
    print('flit',flip)
    random_x = random.random()
    random_y = random.random()
    plt.annotate(
        label,
        xy = (2, x), xytext = (-150*flip*random_x, 150*flip*random_y),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    old_x = x
    
flip=1
for label, x in zip(data.index, data[a3]):
    #calculate distance
    d = ((x-old_x)**2+(1-1)**2)**(.5)
    print(d,label,x)
    #if distance less than thresh then flip the arrow
    if d < thresh: flip*=-1
    print('flit',flip)
    random_x = random.random()
    random_y = random.random()
    plt.annotate(
        label,
        xy = (4, x), xytext = (-150*flip*random_x, 150*flip*random_y),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    old_x = x
# Line Segmentsand Annotation
for p1, p2, c in zip(df[a1], df[a2], df.index):
    newline([0,p1], [2,p2])
for p1, p2, c in zip(df[a2], df[a3], df.index):
    newline([2,p1], [4,p2])

# Lighten borders
plt.gca().spines["top"].set_alpha(.0)
plt.gca().spines["bottom"].set_alpha(.0)
plt.gca().spines["right"].set_alpha(.0)
plt.gca().spines["left"].set_alpha(.0)
plt.show()