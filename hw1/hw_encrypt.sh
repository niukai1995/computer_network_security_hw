#!/bin/zsh
echo “What is the file name?” 
read FILE 
echo “What is the password?”
read PASSWORD
echo 'aes-256-cbc'
(time (openssl enc -aes-256-cbc -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.1.enc"))
echo 'aes-256-ecb'
(time (openssl enc -aes-256-ecb -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.2.enc"))
echo 'des-ecb'
(time (openssl enc -des-ecb -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.3.enc"))
echo 'des-cbc'
(time (openssl enc -des-cbc -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.4.enc"))
echo 'bf-ebc'
(time (openssl enc -bf-ecb -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.5.enc"))
echo 'bf-cbc'
(time (openssl enc -bf-cbc -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.6.enc"))
echo 'camellia-128-cbc'
(time (openssl enc -camellia-128-cbc -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.7.enc"))
echo 'camellia-128-ecb'
(time (openssl enc -camellia-128-ecb -a -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.8.enc"))
