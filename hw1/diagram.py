#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 14:28:15 2019

@author: mark
"""

import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
# Import Data
tmp_data = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/gdppercap.csv")
df = data.sort_values('radio')
data = df
a1 = 'en'
a2 = 'de'
a3 = 'radio'

#left_label = [str(c) + ', '+ str(round(y)) for c, y in zip(df.index, df[a1])]
#right_label = [str(c) + ', '+ str(round(y)) for c, y in zip(df.index, df[a2])]
#klass = ['red' if (y1-y2) < 0 else 'green' for y1, y2 in zip(df[a1], df[a2])]

# draw line
# https://stackoverflow.com/questions/36470343/how-to-draw-a-line-with-matplotlib/36479941
def newline(p1, p2, color='black'):
    ax = plt.gca()
    l = mlines.Line2D([p1[0],p2[0]], [p1[1],p2[1]], color='red' if p1[1]-p2[1] > 0 else 'green', marker='o', markersize=6)
    ax.add_line(l)
    return l

fig, ax = plt.subplots(1,1,figsize=(14,14), dpi= 80)

# Vertical Lines
ax.vlines(x=1, ymin=0, ymax=1.25, color='black', alpha=0.7, linewidth=1, linestyles='dotted')
#ax.vlines(x=3, ymin=0, ymax=2, color='black', alpha=0.7, linewidth=1, linestyles='dotted')

# Points
ax.scatter(y=df[a3], x=np.repeat(1, df.shape[0]), s=10, color='black', alpha=0.7)
#ax.scatter(y=df[a2], x=np.repeat(3, df.shape[0]), s=10, color='black', alpha=0.7)
old_x = old_y = 1e9 # make an impossibly large initial offset
thresh = 0.1 #make a distance threshold
flip=1
for label, x in zip(data.index, data[a3]):
    #calculate distance
    d = d = ((x-old_x)**2+(1-1)**2)**(.5)
    print(d,label,x)
    #if distance less than thresh then flip the arrow
    if d < .1: flip*=-1
    print('flit',flip)
    plt.annotate(
        label,
        xy = (1, x), xytext = (-70*flip*x, 70*flip*x),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    old_x = x
#for label, y in zip(data.index, data[a2]):
#    #calculate distance
#    d = (y-old_y)**2
#
#    #if distance less than thresh then flip the arrow
#    flip = 1
#    if d < .1: flip=-2
#
#    plt.annotate(
#        label,
#        xy = (3, y), xytext = (-20*flip, 20*flip),
#        textcoords = 'offset points', ha = 'right', va = 'bottom',
#        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
#        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
#    old_y = y
# Line Segmentsand Annotation
#for p1, p2, c in zip(df[a1], df[a2], df.index):
#    newline([1,p1], [3,p2])
#    ax.text(1-0.05, p1, c, horizontalalignment='left', verticalalignment='center', fontdict={'size':14})
#    ax.text(3+0.05, p2, c + ', ' + str(round(p2)), horizontalalignment='left', verticalalignment='center', fontdict={'size':14})

# 'Before' and 'After' Annotations
#ax.text(1-0.05, 2, 'BEFORE', horizontalalignment='right', verticalalignment='center', fontdict={'size':18, 'weight':700})
#ax.text(3+0.05, 0, 'AFTER', horizontalalignment='left', verticalalignment='center', fontdict={'size':18, 'weight':700})

# Decoration
#ax.set_title("Slopechart: Comparing GDP Per Capita between 1952 vs 1957", fontdict={'size':22})
#ax.set(xlim=(0,4), ylim=(0,1), ylabel='Mean GDP Per Capita')
#ax.set_xticks([1,3])
#ax.set_xticklabels(["1952", "1957"])
#plt.yticks(np.arange(0, 1, 0.05), fontsize=12)

# Lighten borders
plt.gca().spines["top"].set_alpha(.0)
plt.gca().spines["bottom"].set_alpha(.0)
plt.gca().spines["right"].set_alpha(.0)
plt.gca().spines["left"].set_alpha(.0)
plt.show()