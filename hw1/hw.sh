#!/bin/zsh
echo “What is the file name?” 
read FILE 
echo “What is the output file name?” 
read RESULT 
echo “What is the password?”
read PASSWORD
echo 'aes-256-cbc' #>> $RESULT
(time (openssl enc -aes-256-cbc -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.1.enc")) #|& tee tmp.txt >>  $RESULT
echo 'aes-256-ecb'#>> $RESULT
(time (openssl enc -aes-256-ecb -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.2.enc")) #|& tee tmp.txt >> $RESULT
echo 'des-ecb'#>> $RESULT
(time (openssl enc -des-ecb -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.3.enc")) #|& tee tmp.txt >> $RESULT
echo 'des-cbc'#>> $RESULT
(time (openssl enc -des-cbc -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.4.enc")) #|& tee tmp.txt>> $RESULT
echo 'bf-ecb'#>> $RESULT
(time (openssl enc -bf-ecb -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.5.enc")) #|& tee tmp.txt>> $RESULT
echo 'bf-cbc'#>> $RESULT
(time (openssl enc -bf-cbc -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.6.enc")) #|& tee tmp.txt>> $RESULT
echo 'camellia-128-cbc'#>> $RESULT
(time (openssl enc -camellia-128-cbc -salt -pass pass:$PASSWORD -in $FILE -out "$FILE.7.enc")) #|& tee tmp.txt>> $RESULT
echo 'camellia-128-ecb'#>> $RESULT
(time (openssl enc -camellia-128-ecb -salt -pass pass:$PASSWORD -in $FILE -out "$FILE4.enc")) #|& tee tmp.txt>> $RESULT
