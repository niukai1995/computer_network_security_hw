#!/ bin/zsh
echo " What is the file name ? "
read FILE
while read p
do
	openssl enc -aes-192-cbc -d -pbkdf2 -k $p -in ciphertext.enc -out "result_$p.txt"
done < $FILE
