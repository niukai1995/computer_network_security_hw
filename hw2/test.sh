#!/ bin/zsh
echo " What is the file name ? "
read FILE
while read p
do
	echo $p >> log.file
	openssl enc -aes-192-cbc -d -pbkdf2 -k $p -in ciphertext.enc -out "result.txt" 2>> log.file
done < $FILE
